-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-04-2019 a las 01:47:47
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sig_exam`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parrafos`
--

CREATE TABLE `parrafos` (
  `pa_parrafo` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `parrafos`
--

INSERT INTO `parrafos` (`pa_parrafo`) VALUES
('<p align=\"center\"><img src=\"Images/servos1.jpg\" alt=\"En este tutorial se muestra como funciona un servo y que es lo que hay que saber para utilizarlos en nuestros proyectos.\" width=\"491\" height=\"515\"></p>'),
('<p align=\"center\">&nbsp;</p>'),
('<p class=\"Apartado1\">Como utilizar los servos en nuestros proyectos</p>'),
('<p class=\"Apartado2Simple\">Por Pablo Pompa  </p>'),
('<p>Los servos son sin duda uno de los dispositivos mas &uacute;tiles para cualquier aficionado a la rob&oacute;tica, ya que nos permiten crear toda clase movimientos de          una forma controlada. En este tutorial vamos a mostrar que es un servo, como funciona y como podemos utilizarlos en nuestros proyectos, siempre desde un          punto de vista pr&aacute;ctico y desde el punto de vista de la rob&oacute;tica personal.</p>'),
('<p>Lo que normalmente se denomina servo tambi&eacute;n se conoce como servomotor, motor servo controlado o incluso servorc, y  en cualquier caso hacen referencia          a un sistema compuesto por un motor el&eacute;ctrico,  un sistema de regulaci&oacute;n que act&uacute;a sobre el motor y un sistema de sensor que controla el movimiento del motor.          Es precisamente este sensor el que marca la diferencia con respecto a un motor controlado electr&oacute;nicamente, ya que gracias a la infor'),
('<p>Para entenderlo mejor, veamos un ejemplo. Supongamos que tenemos una plataforma giratoria movida por un motor y  queremos controlar su giro de forma          que la plataforma se pare en determinadas posiciones. Si utilizamos un motor y un circuito de control la &uacute;nica forma que tenemos de hacerlo seria alimentando          el motor durante un tiempo en funci&oacute;n del numero de grados que queremos que gire. Este sistema adem&aacute;s de ser muy poco preciso tiene el gran inconvenien'),
('<p align=\"center\"><img src=\"Images/servos14.jpg\" alt=\"Sitema de control mediante servi con potenciometro externo a la izquierda.\" width=\"425\" height=\"354\"></p>'),
('<p>Si las posiciones de parada fuesen fijas, es decir que tuvieramos unas posiciones marcadas y definidas en las que queremos que la plataforma se detenga,          podr&iacute;amos utilizar alg&uacute;n tipo de sensor como un interruptor final de carrera, o bien un sensor &oacute;ptico que  nos indique cuando la plataforma alcanza          la posici&oacute;n          correcta. En este caso el sistema funcionaria correctamente, pero siempre limitado a las posiciones preestablecidas.</p>'),
('<p>Para un control total y sin restricciones de posici&oacute;n, lo que necesitamos es a&ntilde;adir un sistema sensor que detecte cuando la plataforma se          esta moviendo          y en          que cantidad. Para ello lo          mas sencillo es utilizar un sensor de tipo &oacute;ptico que proporciona impulsos conforme la plataforma gira. De esta forma el sistema de control cuenta con la          informaci&oacute;n necesaria para actuar sobre el motor ya que sabe con exactitud si la plata'),
('<p>Los servos motores pueden ser de muchos tipos y tama&ntilde;os incluyendo los servos hidr&aacute;ulicos, los basados en motores de corriente alterna empleados          en sistemas de gran potencia y los basados en motores de corriente continua que son los mas utilizados en m&aacute;quinas herramientas, robots industriales, sistemas          de producci&oacute;n, etc. El coste de un servo industrial comienza en los 500&euro; y pueden llegar f&aacute;cilmente a varios miles de euros por cada se'),
('<p>Los servo motores que  empleamos en los proyectos de rob&oacute;tica personal son un tipo de servo motor muy especifico que se basan en los mismos principios que          los servos industriales pero con grandes diferencias en cuanto al control y al movimiento que producen y sobre todo al coste de los mismos. De ahora en adelante,          todo lo relacionado con servos se refiere a este tipo de servos.</p>'),
('<p class=\"Menu1\">&iquest;Que es un servo?</p>'),
('<p>Los servos RC se inventaron para utilizarse como elementos de control en modelos teledirigidos. Los primeros servos empleados en coches y barcos de radio          control consist&iacute;an en motores que ten&iacute;an un sistema mec&aacute;nico que controlaba la posici&oacute;n del mismo en funci&oacute;n de los pulsos recibidos v&iacute;a radio. El sistema          era muy b&aacute;sico y era secuencial, el motor pasaba por las diferentes posiciones mecanicas hasta llegar a la posici&oacute;'),
('<p>El hecho de que el servo motor se controle v&iacute;a radio, ha sido decisivo a la hora de establecer la forma de controlarlos. Hab&iacute;a que buscar la forma de enviar          una se&ntilde;al v&iacute;a radio que controlara el movimiento del servo y adem&aacute;s hab&iacute;a que mandar varias se&ntilde;ales a la vez, ya que la          mayor&iacute;a de los coches y aviones necesitan varios servos para ser controlado.</p>'),
('<p align=\"center\"><img src=\"Images/servos16.jpg\" alt=\"Relaci&oacute;n entre el ancho del pulso de control y la posici&oacute;n del servo.\" width=\"500\" height=\"320\"></p>'),
('<p>Para solventar este hecho, lo que se ha hecho es crear un sistema de control basado en el ancho de un pulso para controlar la posici&oacute;n del motor. Este pulso          que normalemente es de 1,5 ms mantiene el servo en la posici&oacute;n centrada. Si el pulso es mas corto, por ejemplo 1 ms el servo gira a la izquierda, si el          pulso es mayor, por ejemplo 2 ms, el servo gira a la derecha. El movimiento del servo es proporcional al pulso que se le aplica. Otra particularidad que    '),
('<p>Los servos modernos dejan de controlar el motor, tan pronto como se dejan de mandar los pulsos de control. Por esos para controlar un servo hay que mandar          los pulsos de control una 50 veces por segundo, con un margen del 20 % aproximadamente. Si pasa mas tiempo sin mandar los pulsos el servo entra en reposo          y como consecuencia su consumo baja a apenas 8 mA, lo que puede ser muy interesante para algunos proyectos.</p>'),
('<p>Otra particularidad que tienen los servos de radio control es que su movimiento esta limitado en la mayor&iacute;a de los casos a 180 grados. En los sistemas originales          controlados v&iacute;a radio, el rango de movimiento es de 90 grados, es decir 45 grados hacia cada lado desde la posici&oacute;n central ya que el ancho del pulso va          desde los 900 a los 2100 milisegundos. Esto es suficiente para mover los difrentes mandos de los modelos, como son el tim&oacute;n, la direcci&'),
('<p align=\"center\"><img src=\"Images/servos17.jpg\" alt=\"El pulso de control del servo se tiene que mandar cada 20 ms o lo que es lo mismo 50 veces por segundo.\" width=\"600\" height=\"160\"></p>'),
('<p>Esta limitaci&oacute;n en el movimiento tubo como  consecuencia que el sistema sensor de la posici&oacute;n se pudiera reducir a un simple potenciometro, lo que simplificaba          el servo desde el punto de vista el&eacute;ctrico y adem&aacute;s abarata su coste y su peso. El potenciometro se encuentra conectado mecanicamente al eje de salida del          servo de forma los dos se mueven a la vez. De ah&iacute; la dificultad de hacer que un servo gire  mas de 270 grados, ya que mecanicamen'),
('<p>Desde el punto de vista el&eacute;ctrico, el servo es un circuito que se encuentra en equilibrio. El equilibrio se mantiene entre el ancho del pulso de control          y la se&ntilde;al que recibe del potenciometro. Cuado se manda un pulso de control, el equilibrio se descompensa y el circuito mueve el motor y el potenciometro          hasta que consigue que la se&ntilde;al que procede del potenciometro equilibre de nuevo el circuito. Una vez que el eje alcanza la posici&oacute;n de equilibr'),
('<p>De forma generica, la mayor&iacute;a de los servos funcionan con tensiones comprendidas entre los 4,8V y los 6V siendo 6 V la tensi&oacute;n m&aacute;xima recomendada y en la          que se obtiene mas potencia, rendimiento y velocidad. Algunos servos admiten 7,2V, pero no es lo corriente y pueden da&ntilde;ar los servos, por lo que es          necesario asegurarse antes de emplear esta tensi&oacute;n.</p>'),
('<p class=\"Apartado1\">Tipos de servos</p>'),
('<p>Hasta hace muy poco tiempo la &uacute;nica clasificaci&oacute;n que hab&iacute;a de los servos era la relativa a su tama&ntilde;o. Todos los servos funcionaban    de la misma forma y lo &uacute;nico que los diferenciaba era el tama&ntilde;o de los mismos. Dentro de esta clasificaci&oacute;n estan los sevos est&aacute;ndar que    incluye a la mayor&iacute;a de los servos y que se trata de servos cuyas medidas son 40 x 20 x 37 mm. Las medidas var&iacute;an en algunas decima de unos modelos a ot'),
('<p align=\"center\"><img src=\"Images/servos18.jpg\" alt=\"Medidas de un servo estandard.\" width=\"370\" height=\"420\"></p>'),
('<p>Una caracter&iacute;stica de los servos estandar es la posici&oacute;n del eje de salida, si bien las medidas de la altura del servo puede variar de un modelo, la distancia    entre las fijaciones y el plato del servo suele ser muy parecida en la mayor&iacute;a de los modelos. Ejemplos de servo de tama&ntilde;o est&aacute;ndar son el <a href=\"S330165.htm\">HS422</a> ,    el <a href=\"S330195.htm\">HS1422</a>,    o el <a href=\"S330173.htm\">HS5980</a>. La potencia de estos servos varia entre los 3'),
('<p>Otros servos de mayor tama&ntilde;o son los servos de escala 1/4 que se empleaban en modelos de coches a escala 1:4 y en general donde hacen falta servos mas    robustos y con mayor potencia. Estos servos se empleaban sobre todo en modelos en donde el peso no era tan problematico y sobre todo hacia falta mucha potencia    como en el caso de la direcci&oacute;n de los coches de 4x4 de gran potencia o los barcos a vela grandes en los que la tensi&oacute;n. de las velas puede ser bastante alta. '),
('<p>Los servos de menor tama&ntilde;o puede ser de diferentes tipos y tama&ntilde;os, sin que existan un estandar definido. En principio los servos de menor tama&ntilde;o    se empleaban sobre todo en los aviones, en donde el peso y el tama&ntilde;o es muy critico. Hay un caso muy concreto en los servos que se emplean para los trenes    de aterrizajes, por un lado tienen que girar 90 grados y por otro tienen que ser muy peque&ntilde;os, ya que en la mayor&iacute;a de los casos van alojados en las'),
('<p align=\"center\"><img src=\"Images/servos7.jpg\" alt=\"Diferentes platos y brazos de sujeci&oacute;n de servos.\" width=\"548\" height=\"389\"></p>'),
('<p>Hoy en d&iacute;a existen una gran variedad de servos de diferentes tama&ntilde;os, aplicaciones, tensiones y forma de control por lo que seria posible establecer    otros sistemas de clasificaci&oacute;n. No obstante la segunda forma mas frecuente de clasificar a los servos se basa en la forma de control interna que sea digital o    anal&oacute;gica.</p>'),
('<p>Hasta hace muy poco, todos los servos eran anal&oacute;gicos, es decir que se basaban en un circuito electr&oacute;nico cuyo control es completamente anal&oacute;gico, basado en comparadores    y amplificadores operacionales que siempre funcionan de la misma forma. En cambio con la aparici&oacute;n de los servos digitales, se han conseguido grandes avances    tanto en prestaciones, como en posibilidades de control. En principio, los servos digitales funcionan y se controlan de la misma forma '),
('<p align=\"center\"><img src=\"Images/servos9.jpg\" alt=\"Aspecto de los engranajes metalicos de un servo digital de 24 kilos.\" width=\"436\" height=\"263\"></p>'),
('<p>Una caracter&iacute;stica difereciadora entre los servos, es la relativa al eje de salida. Durante muchos a&ntilde;os el mercado estuvo liderado por dos marcas.    Una japonesa Futaba y la otra coreana Hitec que ten&iacute;an modelos muy similares y cuya principal diferencia eran las estr&iacute;as del eje de salida que son incompatibles    entre si. Esto ha provocado que a la hora de crear, accesorios, fijaciones, platos, etc para los servos, es necesario distinguir entre los servos tipo Fut'),
('<p align=\"center\"><img src=\"Images/servos8.jpg\" alt=\"Detalle de las estrias de un servo Hitec.\" width=\"256\" height=\"248\"></p>'),
('<p>Otra diferencia aunque menor est&aacute; en los conectores el&eacute;ctricos de los servos. La mayor&iacute;a de los conectores utilizan un sistema de 3 hilos acabados en un    conector con separaci&oacute;n entre pines de 0,1 pulgadas (2,54 mm) que encajan sin problemas en los conectores tipo poste que tienen los circuitos de control de servos.    Otro caso diferente es cuando se conectan en los receptores de radio control, ya que algunos servos llevan una leng&uuml;eta de polarizaci&oacute;'),
('<p align=\"center\"><img src=\"Images/servos19.jpg\" width=\"480\" height=\"390\"></p>'),
('<p class=\"Apartado1\">&iquest;Como Controlar los Servos?</p>'),
('<p>Hay cuatro formas b&aacute;sicas de controlar el movimiento de los servos. El sistema empleado en cada caso depender&aacute; principalmente de nuestras necesidades    y posibilidades. Comentaremos a continuaci&oacute;n cada uno de ellos para ver las ventajas e inconvenientes.</p>'),
('<p class=\"Apartado2\">Control manual v&iacute;a radio.</p>'),
('<p>Este metodo es el cl&aacute;sico sistema de Radio Control que se emplea para controlar aviones y coches de modelismo. El sistema emplea un receptor en el lado de los    servos y un transmisor de radio para hacer el control. </p>'),
('<p align=\"center\"><img src=\"Images/servos4.jpg\" alt=\"Ejemplo de transmisores de radio control para controlar servos.\" width=\"585\" height=\"420\"></p>'),
('<p>Los transmisores puede ser de muchos tipos pero los mas frecuentes son los mostrados en la imagen de arriba en la que se puede ver las palancas de control tipo    joystick que facilitan el control y permiten realizar movimientos con gran precisi&oacute;n Hay transmisores anal&oacute;gicos (izquierda) y digitales (derecha), al igual que    los hay con m&uacute;ltiples funciones avanzadas, como mezcladores de canales, controles de ganancia, memorias, etc. Los mas economico suelen ser sistemas d'),
('<p align=\"center\"><img src=\"Images/servos3.jpg\" alt=\"Los receptores de radio permiten controlar varios servos a distancia.\" width=\"608\" height=\"332\"></p>'),
('<p>&nbsp;</p>'),
('<p>Los receptores por su parte son unas unidades muy compactas y f&aacute;cil de utilizar. Lo &uacute;nico que hay que hacer es conectar los servos en cada uno de los canales          que se quiera controlar y conectar una bater&iacute;a de 4,8V a 6V en la ranura marcada como &quot;B&quot;.</p>'),
('<p>El sistema v&iacute;a radio tiene dos caracter&iacute;sticas fundamentales que lo hacen &uacute;nicos. La primera es el hecho de no utilizar cables, lo cual en muchos casos          es una gran ventaja, aunque en otras ocasiones puede ser un problema, puesto que un sistema v&iacute;a radio siempre es susceptible de tener interferencias. La          segunda caracter&iacute;stica es el hecho de que se controla completamente a mano. Por ejemplo para controlar un avi&oacute;n o un coche, la mejor'),
('<p align=\"center\"><img src=\"Images/servos5.jpg\" alt=\"Las palancas de control tipo joystick permiten controlar dos servos con gran precisi&oacute;n y velocidad.\" width=\"525\" height=\"417\">        </p>'),
('<p class=\"Apartado2\">Control mediante circuito controlador de servos.</p>'),
('<p>Este sistema emplea circuitos especializados en el control de servos. Este tipo de circuitos se caracterizan porque se encargan de generar las se&ntilde;ales          necesarias para controlar los servos. Lo &uacute;nico que tenemos que hacer es decirle al controlador la posici&oacute;n a la que queremos que se mueva cada servo y este          se encarga de todo lo demas. Como vimos al principio, los servos necesitan recibir los pulsos de control 50 veces por segundo y si adem&aacute;s tenemo'),
('<p align=\"center\"><img src=\"Images/servos2.jpg\" width=\"539\" height=\"501\"></p>'),
('<p>Actualmente hay muchos tipos de circuitos controladores de servos que var&iacute;an tanto en el numero de canales como en el interfaz que utilizan. Los mas corrientes          y universales son los controladores con conexion serie, que reciben las ordenes de control de movimiento a trav&eacute;s de un puerto serie. Algunos modelos muy          conocidos son el mini SSCII de 8 canales que fue uno de los pioneros y ampliamente utilizado durante muchos a&ntilde;os. Una versi&oacute;n avanzada y '),
('<p>Una caso especial es el circuito controlador SD84 que destaca por que es capaz de controlar hasta 84 canales de forma simultanea. Este circuito tiene conexion          USB aunque en el PC finalmente aparece como un puerto serie mas, por lo que el control desde el PC se hace mandando ordenes por un puerto serie. Este circuito          adem&aacute;s puede utilizar los canales como puertos de entrada y salida digital o incluso 36 de los canales pueden funcionar como entradas anal&oacute;gicas. G'),
('<p>Tambi&eacute;n hay  circuito controladores de servos con otros tipos de conexiones como es el caso de los m&oacute;dulos SD20 y SD21 que se controlan por bus I2C.          El primero consiste          en un          microcontrolador          PIC pre programado y que es capaz de manejar 20 servos. La idea en este caso es la de utilizarlo dentro de nuestro propio circuito como un coprocesador del          microcontrolador principal, teniendo que ocuparnos de las conexiones, la alimentaci&oacute'),
('<p>Hacer menci&oacute;n especial al controlador de servos SSC-32 que actualmente es uno de los controladores mas avanzado y completos del mercado y con un precio          realmente atractivo. Otra ventaja es que este controlador se puede utilizar con todo el software tanto gratuito como comercial que hay escrito para          el MiniSSC8, adem&aacute;s de contar con programas comerciales propios especialmente desarrollados para el y que sacan partido de las caracter&iacute;sticas avanzadas      '),
('<p align=\"center\"><a href=\"S300430.htm\"><img src=\"Images/s300430.jpg\" alt=\"El controlador MRC3024 tiene su propio microcontrolador programable y es capaz de controlar hasta 24 servos.\" width=\"320\" height=\"240\" border=\"0\"></a></p>'),
('<p>Un caso especial es el circuito de control que utiliza el robot Robonova y que puede adquirirse por separado de este es el MRC3024 que es capaz de controlar          24 servos y que cuenta con su propio controlador en la propia placa. Este circuito puede emplear todo el software del robonova incluido el lenguaje robobasic          que es muy f&aacute;cil de programar. Adem&aacute;s si se utiliza con servos compatibles con el protocolo HMI es capaz de leer la posici&oacute;n real de los servos'),
('<p class=\"Apartado2\">Control Mediante microcontrolador.</p>'),
('<p>Otra forma de controlar los servos es utilizar directamente un microcontrolador para generar las se&ntilde;ales de control de los servos. La principal ventaja          de este sistema es que es economico y simple ya que solo es necesario un puerto de salida del microcontrolador para hacer el movimiento. Este es el tipico          caso de robot que solo tienen uno o dos servos que se emplean para mover el robot. El microcontrolador utiliza los sensores para leer el entorno y genera          lo'),
('<p align=\"center\"><a href=\"S310162.htm\"><img src=\"Images/s310162.jpg\" alt=\"mini Atom Board es un circuito que permite conectar cualquier microcontrolador de 24 patillas y controlar servos y otros dispositivos.\" width=\"320\" height=\"240\" border=\"0\"></a></p>'),
('<p>Las mimas razones que se presentan como una ventaja pueden ser un inconveniente seg&uacute;n el numero de servos o la complejidad del proyecto. Cuando se controlan          muchos servos suele ser mas practico utilizar un circuito controlador de servos en lugar del control directo. Adem&aacute;s el empleo de un microcontrolador nos          obliga ha tener que hacer por nosotros mismos todo el programa de control y la electr&oacute;nica, en lugar de utilizar programas y circuitos que ya sean '),
('<p class=\"Apartado2\">Controlador especifico.</p>'),
('<p>Los controladores espec&iacute;ficos son circuitos especializados en el control de servos, pero de una forma concreta o para una aplicaci&oacute;n especifica. En este          grupo de entran los controladores de movimientos X Y que con palanca tipo joystick que se utilizan por ejemplo para mover un cabezal Pan and Tilt (movimiento          horizontal y vertical de una camara) . Este tipo de controlador incluye en una unidad muy compacta toda la electr&oacute;nica y el mando de control de for'),
('<p align=\"center\"><a href=\"S310182.htm\"><img src=\"Images/s310182.JPG\" width=\"320\" height=\"240\" border=\"0\"></a></p>'),
('<p>Otros controladores espec&iacute;ficos son los secuenciadores de movimientos con servos que se suelen emplear en robotr&oacute;nica.          En este caso se trata de un circuito microcontrolador que es capaz de manejar varios servos (de 4 a 16 normalmente)          y que se caracterizan porque son capaces de repetir secuencias de movimiento de forma totalmente aut&oacute;noma sin necesidad          de ordenador. En algunas ocasiones el propio circuito es capaz de memorizar los movimientos di'),
('<p align=\"center\"><a href=\"S310172.htm\"><img src=\"Images/s310172.JPG\" width=\"320\" height=\"240\" border=\"0\"></a></p>'),
('<p>Hay otros circuitos que hacen cosas parecidas pero con caracter&iacute;sticas mas o menos especificas y que ya vienen a          ser una mezcla o combinaci&oacute;n de los anteriores o que simplemente permiten mover uno o dos servos de forma manual          sin necesidad de mas caracter&iacute;sticas adicionales.</p>'),
('<p align=\"center\"><a href=\"S310192.htm\"><img src=\"Images/s310192.JPG\" alt=\"Este controlador permite mover 2 servos de foma manual con solo girar los mandos de control.\" width=\"320\" height=\"240\" border=\"0\"></a> <a href=\"S310195.htm\"><img src=\"Images/s310195.jpg\" alt=\"El controlador mas simple, capaz de controlar un solo servo, bien mediante un potenciometro o directamente por pulsadores.\" width=\"320\" height=\"240\" border=\"0\"></a></p>'),
('<p>&nbsp;</p>'),
('<p class=\"Apartado2\">Aplicaciones de los Servos.</p>'),
('<p>La aplicaci&oacute;n de los servos es simple, mover con fuerza y precisi&oacute;n una palanca. No obstante con solo 180 grados y un poco de imaginaci&oacute;n          se pueden conseguir hacer m&aacute;quinas verdaderamente complejas con movimientos sincronizados y capaces de realizar m&uacute;ltiples tareas. Ejemplos de ellos son el          Robot Robonova, capaz de andar y dar volteretas gracias a que mueve 16 servos de forma simultanea. Otro ejemplo son los brazos robots, que con cinco o '),
('<p align=\"center\"><img src=\"Images/s300114.gif\" alt=\"Brazo robot de 6 ejes movido por servos\" width=\"320\" height=\"240\"><img src=\"Images/s300400.jpg\" alt=\"Robonova el robot humanoide que se mueve como una persona.\" width=\"320\" height=\"240\"></p>'),
('<p>&nbsp;</p>'),
('<p>Otras aplicaciones incluyen muchas clases de componentes mecanicos, articulaciones, accionadores y elementos m&oacute;viles que se pueden emplear para construir          artefactos de toda indole sin mas limite que la imaginaci&oacute;n. Para ello los servos cuentan con un buen surtido de soportes en forma de pletinas met&aacute;licas          que permiten          unirlos entre si, creando ensamblajes m&oacute;viles y estructuras complejas que permiten hacer realidad cuantas aplicaciones sea'),
('<p>&nbsp;</p>'),
('<p align=\"center\"><img src=\"Images/s360225e.jpg\" alt=\"Construccion de esamblajes articulados\" width=\"320\" height=\"240\"><img src=\"Images/s360235c.jpg\" alt=\"Sistemas de ruedas y direcci&oacute;n\" width=\"320\" height=\"240\"></p>'),
('<p>&nbsp;</p>'),
('<p align=\"center\"><img src=\"Images/s360240b.jpg\" alt=\"Patas articuladas\" width=\"320\" height=\"240\"><img src=\"Images/servos20.jpg\" alt=\"Robots ara&ntilde;a\" width=\"320\" height=\"240\"></p>'),
('<p align=\"center\"><img src=\"Images/servos21.jpg\" alt=\"Brazos robot\" width=\"320\" height=\"240\"><img src=\"Images/servos22.jpg\" alt=\"Pinzas y sistemas de sujecion\" width=\"320\" height=\"240\"></p>'),
('<p>&nbsp;</p>'),
('<p><a href=\"http://www.superinventos.com/\"><img src=\"Images/Logosuper1.gif\" width=\"430\" height=\"62\" border=\"0\" alt=\"Camaras, cctv, televigilancia, dom&oacute;tica, c&aacute;maras esp&iacute;as, transmisores, video por ip, video remoto\"></a></p>'),
('<p>Actualizada el 03/04/2019 &copy; 2002 - 2019 <b><i>INTPLUS</i> &reg;</b>. Todos los derechos reservados</p>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pa_img`
--

CREATE TABLE `pa_img` (
  `pa_imgenes` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pa_img`
--

INSERT INTO `pa_img` (`pa_imgenes`) VALUES
('<img src=\"Images/LogoRobot2.gif\" width=\"670\" height=\"89\" border=\"0\" alt=\"Robotica Facil. Robots, kits, circuitos, sensores, motores, etc..\">'),
('<img src=\"Images/RobotSR1.gif\" alt=\"SR1 Robot Multifuncional \" width=\"78\" height=\"62\" border=\"0\">'),
('<img src=\"Images/menu1ar.gif\" alt=\"Kits Robots\" name =\"Robots\" width=\"196\" height=\"32\" border=\"0\">'),
('<img src=\"Images/menu2ar.gif\" alt=\"Circuitos electr&oacute;nicos\" name=\"Circuitos\" width=\"196\" height=\"32\" border=\"0\">'),
('<img src=\"Images/menu3ar.gif\" alt=\"Sensores robotica\" name=\"Sensores\" width=\"196\" height=\"32\" border=\"0\">'),
('<img src=\"Images/menu4ar.gif\" alt=\"Motores y servos para robots\" name=\"Motores\" width=\"196\" height=\"32\" border=\"0\">'),
('<img src=\"Images/menu5ar.gif\" alt=\"Ruedas y Orugas para robots\" name=\"Ruedas\" width=\"196\" height=\"32\" border=\"0\">'),
('<img src=\"Images/menu6ar.gif\" alt=\"Soportes y hardware\" name =\"Soportes\" width=\"196\" height=\"32\" border=\"0\">'),
('<img src=\"Images/menu7ar.gif\" alt=\"Tornillos y separadores\" name=\"Tornillos\" width=\"196\" height=\"32\" border=\"0\">'),
('<img src=\"Images/menu8ar.gif\" alt=\"Libros robotica\" name=\"Libros\" width=\"196\" height=\"32\" border=\"0\">'),
('<img src=\"Images/menu9ar.gif\" alt=\"Radio Modem, Transmisores de Vídeo\" name=\"transmisores\" width=\"196\" height=\"32\" border=\"0\">'),
('<img src=\"Images/menu10ar.gif\" alt=\"Robots Fotograficos\" name=\"Fotorobotica\" width=\"196\" height=\"32\" border=\"0\">'),
('<img src=\"Images/menu11ar.gif\" alt=\"Fuentes de Alimentacion\" name =\"Alimentacion\" width=\"196\" height=\"32\" border=\"0\">'),
('<img src=\"Images/menu12ar.gif\" alt=\"Componentes electr&oacute;nicos\" name=\"Electronica\" width=\"196\" height=\"32\" border=\"0\">'),
('<img src=\"Images/menu13ar.gif\" alt=\"Varios robotica\" name=\"Varios\" width=\"196\" height=\"32\" border=\"0\">'),
('<img src=\"Images/menu14ar.gif\" alt=\"Proyectos de Robotica\" name=\"Proyectos\" width=\"196\" height=\"32\" border=\"0\">'),
('<img src=\"Images/menu15ar.gif\" alt=\"Tutoriales de Robotica\" name=\"Tutoriales\" width=\"196\" height=\"32\" border=\"0\">'),
('<img src=\"Images/barra.gif\" width=\"1000\" height=\"9\">'),
('<img src=\"Images/barra.gif\" width=\"1000\" height=\"9\">'),
('<img src=\"Images/servos1.jpg\" alt=\"En este tutorial se muestra como funciona un servo y que es lo que hay que saber para utilizarlos en nuestros proyectos.\" width=\"491\" height=\"515\">'),
('<img src=\"Images/servos14.jpg\" alt=\"Sitema de control mediante servi con potenciometro externo a la izquierda.\" width=\"425\" height=\"354\">'),
('<img src=\"Images/servos16.jpg\" alt=\"Relaci&oacute;n entre el ancho del pulso de control y la posici&oacute;n del servo.\" width=\"500\" height=\"320\">'),
('<img src=\"Images/servos17.jpg\" alt=\"El pulso de control del servo se tiene que mandar cada 20 ms o lo que es lo mismo 50 veces por segundo.\" width=\"600\" height=\"160\">'),
('<img src=\"Images/servos18.jpg\" alt=\"Medidas de un servo estandard.\" width=\"370\" height=\"420\">'),
('<img src=\"Images/servos7.jpg\" alt=\"Diferentes platos y brazos de sujeci&oacute;n de servos.\" width=\"548\" height=\"389\">'),
('<img src=\"Images/servos9.jpg\" alt=\"Aspecto de los engranajes metalicos de un servo digital de 24 kilos.\" width=\"436\" height=\"263\">'),
('<img src=\"Images/servos8.jpg\" alt=\"Detalle de las estrias de un servo Hitec.\" width=\"256\" height=\"248\">'),
('<img src=\"Images/servos19.jpg\" width=\"480\" height=\"390\">'),
('<img src=\"Images/servos4.jpg\" alt=\"Ejemplo de transmisores de radio control para controlar servos.\" width=\"585\" height=\"420\">'),
('<img src=\"Images/servos3.jpg\" alt=\"Los receptores de radio permiten controlar varios servos a distancia.\" width=\"608\" height=\"332\">'),
('<img src=\"Images/servos5.jpg\" alt=\"Las palancas de control tipo joystick permiten controlar dos servos con gran precisi&oacute;n y velocidad.\" width=\"525\" height=\"417\">'),
('<img src=\"Images/servos2.jpg\" width=\"539\" height=\"501\">'),
('<img src=\"Images/s300430.jpg\" alt=\"El controlador MRC3024 tiene su propio microcontrolador programable y es capaz de controlar hasta 24 servos.\" width=\"320\" height=\"240\" border=\"0\">'),
('<img src=\"Images/s310162.jpg\" alt=\"mini Atom Board es un circuito que permite conectar cualquier microcontrolador de 24 patillas y controlar servos y otros dispositivos.\" width=\"320\" height=\"240\" border=\"0\">'),
('<img src=\"Images/s310182.JPG\" width=\"320\" height=\"240\" border=\"0\">'),
('<img src=\"Images/s310172.JPG\" width=\"320\" height=\"240\" border=\"0\">'),
('<img src=\"Images/s310192.JPG\" alt=\"Este controlador permite mover 2 servos de foma manual con solo girar los mandos de control.\" width=\"320\" height=\"240\" border=\"0\">'),
('<img src=\"Images/s310195.jpg\" alt=\"El controlador mas simple, capaz de controlar un solo servo, bien mediante un potenciometro o directamente por pulsadores.\" width=\"320\" height=\"240\" border=\"0\">'),
('<img src=\"Images/s300114.gif\" alt=\"Brazo robot de 6 ejes movido por servos\" width=\"320\" height=\"240\">'),
('<img src=\"Images/s300400.jpg\" alt=\"Robonova el robot humanoide que se mueve como una persona.\" width=\"320\" height=\"240\">'),
('<img src=\"Images/s360225e.jpg\" alt=\"Construccion de esamblajes articulados\" width=\"320\" height=\"240\">'),
('<img src=\"Images/s360235c.jpg\" alt=\"Sistemas de ruedas y direcci&oacute;n\" width=\"320\" height=\"240\">'),
('<img src=\"Images/s360240b.jpg\" alt=\"Patas articuladas\" width=\"320\" height=\"240\">'),
('<img src=\"Images/servos20.jpg\" alt=\"Robots ara&ntilde;a\" width=\"320\" height=\"240\">'),
('<img src=\"Images/servos21.jpg\" alt=\"Brazos robot\" width=\"320\" height=\"240\">'),
('<img src=\"Images/servos22.jpg\" alt=\"Pinzas y sistemas de sujecion\" width=\"320\" height=\"240\">'),
('<img src=\"Images/barra.gif\" width=\"1000\" height=\"9\">'),
('<img src=\"Images/barra.gif\" width=\"1000\" height=\"9\">'),
('<img src=\"Images/Logosuper1.gif\" width=\"430\" height=\"62\" border=\"0\" alt=\"Camaras, cctv, televigilancia, dom&oacute;tica, c&aacute;maras esp&iacute;as, transmisores, video por ip, video remoto\">');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pa_url`
--

CREATE TABLE `pa_url` (
  `pa_urls` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pa_url`
--

INSERT INTO `pa_url` (`pa_urls`) VALUES
('<a href=\"Default.htm\"><img src=\"Images/LogoRobot2.gif\" width=\"670\" height=\"89\" border=\"0\" alt=\"Robotica Facil. Robots, kits, circuitos, sensores, motores, etc..\"></a>'),
('<a href=\"sr1_Robot.htm\"><img src=\"Images/RobotSR1.gif\" alt=\"SR1 Robot Multifuncional \" width=\"78\" height=\"62\" border=\"0\"></a>'),
('<a href=\"default.htm\"><span class=\"Blanco\">P&aacute;gina Principal</span></a>'),
('<a href=\"novedades.htm\"><span class=\"Blanco\">Novedades</span></a>'),
('<a href=\"garantia.htm\"><span class=\"Blanco\">Ayuda</span></a>'),
('<a href=\"cgi-bin/robocarrito.asp?modo=449&amp;IdRef=ver\"><span class=\"Blanco\">Ver Carrito</span></a>'),
('<a href=\"catalogo.htm\"><span class=\"Blanco\">Cat&aacute;logo</span></a>'),
('<a href=\"S330165.htm\">HS422</a>'),
('<a href=\"S330195.htm\">HS1422</a>'),
('<a href=\"S330173.htm\">HS5980</a>'),
('<a href=\"S300430.htm\"><img src=\"Images/s300430.jpg\" alt=\"El controlador MRC3024 tiene su propio microcontrolador programable y es capaz de controlar hasta 24 servos.\" width=\"320\" height=\"240\" border=\"0\"></a>'),
('<a href=\"S310162.htm\"><img src=\"Images/s310162.jpg\" alt=\"mini Atom Board es un circuito que permite conectar cualquier microcontrolador de 24 patillas y controlar servos y otros dispositivos.\" width=\"320\" height=\"240\" border=\"0\"></a>'),
('<a href=\"S310182.htm\"><img src=\"Images/s310182.JPG\" width=\"320\" height=\"240\" border=\"0\"></a>'),
('<a href=\"S310172.htm\"><img src=\"Images/s310172.JPG\" width=\"320\" height=\"240\" border=\"0\"></a>'),
('<a href=\"S310192.htm\"><img src=\"Images/s310192.JPG\" alt=\"Este controlador permite mover 2 servos de foma manual con solo girar los mandos de control.\" width=\"320\" height=\"240\" border=\"0\"></a>'),
('<a href=\"S310195.htm\"><img src=\"Images/s310195.jpg\" alt=\"El controlador mas simple, capaz de controlar un solo servo, bien mediante un potenciometro o directamente por pulsadores.\" width=\"320\" height=\"240\" border=\"0\"></a>'),
('<a href=\"Default.htm\">Ir a la P&aacute;gina Principal de www.SuperRobotica.com</a>'),
('<a href=\"http://www.superinventos.com/\"><img src=\"Images/Logosuper1.gif\" width=\"430\" height=\"62\" border=\"0\" alt=\"Camaras, cctv, televigilancia, dom&oacute;tica, c&aacute;maras esp&iacute;as, transmisores, video por ip, video remoto\"></a>');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
